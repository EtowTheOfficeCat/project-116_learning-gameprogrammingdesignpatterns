﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cube : MonoBehaviour, ICube
{
    public void Move(Vector3 dir)
    {
        transform.position += dir;
    }

    public void Scale(float factor)
    {
        transform.localScale *= factor;
    }
}
