﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private Stack<Icommands1> undoStack = new Stack<Icommands1>();
    private Stack<Icommands1> redoStack = new Stack<Icommands1>();

    [SerializeField] private cube startCube;

    private Imoveble curMoveble;
    private ITransformable curtranformable;
    public Imoveble test;
    private Camera cam;


    private void Awake()
    {
        cam = Camera.main;
        if(curMoveble == null)
        {
            Debug.LogWarning("Please assign default Imoveble");
        }
        curMoveble = startCube.GetComponent<Imoveble>();
    }
    private void Update()
    {
        if(Input.GetMouseButtonDown(0) && Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition), out RaycastHit hit))
        {
            var moveble = hit.collider.GetComponent<Imoveble>();
            if(moveble != null)
            {
                curMoveble = moveble;
            }
        }
    }

    private void OnGUI()
    {
        if(GUILayout.Button("Move Left"))
        {
            redoStack.Clear();
            undoStack.Push(new MoveCommand1(Vector3.left, curMoveble));
            undoStack.Peek().Execute();
        }

        if (GUILayout.Button("Move Right"))
        {
            redoStack.Clear();
            undoStack.Push(new MoveCommand1(Vector3.right, curMoveble));
            undoStack.Peek().Execute();
        }

        if (GUILayout.Button("Move Up"))
        {
            redoStack.Clear();
            undoStack.Push(new MoveCommand1(Vector3.up, curMoveble));
            undoStack.Peek().Execute();
        }

        if (GUILayout.Button("Move Down"))
        {
            redoStack.Clear();
            undoStack.Push(new MoveCommand1(Vector3.down, curMoveble));
            undoStack.Peek().Execute();
        }

        if (GUILayout.Button("Undo"))
        {
            if (undoStack.Count < 1) { return; }
            redoStack.Push(item: undoStack.Pop());
            redoStack.Peek().Undo();
        }

        if (GUILayout.Button("Redo"))
        {
            if (redoStack.Count < 1) { return; }
            undoStack.Push(item: redoStack.Pop());
            undoStack.Peek().Execute();
        }

        //if (GUILayout.Button("ScaleUp"))
        //{
        //    redoStack.Clear();
        //    undoStack.Push(new ScaleCommand( 2f, curtranformable));
        //    undoStack.Peek().Execute();
        //}

        //if (GUILayout.Button("ScaleDown"))
        //{
        //    redoStack.Clear();
        //    undoStack.Push(new ScaleCommand(2f, curtranformable));
        //    undoStack.Peek().Execute();
        //}
    }
}
