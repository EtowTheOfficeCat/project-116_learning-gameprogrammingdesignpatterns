﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCommand1 :  Icommands1
{
    private Vector3 direction;
    private Imoveble _moveble;

    public MoveCommand1(Vector3 direction, Imoveble moveble)
    {
        this.direction = direction;
        _moveble = moveble;
    }
    public void Execute()
    {
        _moveble.Move(direction);
    }

    public void Undo()
    {
        _moveble.Move(-direction);
    }

    public void Redo()
    {
        _moveble.Move(direction);
    }
}
