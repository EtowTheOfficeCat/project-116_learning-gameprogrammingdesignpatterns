﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Icommands1
{
    void Execute();
    void Undo();
    void Redo();
}

public interface Imoveble
{
    void Move(Vector3 dir);
}

public interface IScalable
{
    void Scale(float factor);
}

public interface ICube : Imoveble, IScalable
{

}

public interface ITransformable
{

}
    
