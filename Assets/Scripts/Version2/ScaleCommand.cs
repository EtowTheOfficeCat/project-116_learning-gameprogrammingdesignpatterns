﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleCommand : Icommands1
{
    private IScalable _scalable;
    private float _factor;

    public ScaleCommand(float factor, IScalable scalable)
    {
        _scalable = scalable;
        _factor = factor;
    }
    public void Execute()
    {
        _scalable.Scale(_factor);
    }

    public void Undo()
    {
        _scalable.Scale(1 / _factor);
    }

    public void Redo()
    {
        _scalable.Scale(_factor);
    }

    
}
