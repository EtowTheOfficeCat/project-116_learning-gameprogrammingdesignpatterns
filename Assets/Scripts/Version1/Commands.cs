﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Icommands
{
    void Execute();
    void Undo();
    void Redo();
}
