﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    Icommands moveLeft;
    Icommands moveRight;
    Icommands moveUp;
    Icommands moveDown;
    Stack<Icommands> history = new Stack<Icommands>();
    Stack<Icommands> undoHistory = new Stack<Icommands>();

    private void Start()
    {
        moveLeft = new MoveCommand(Vector3.left, transform);
        moveRight = new MoveCommand(Vector3.right, transform);
        moveUp = new MoveCommand(Vector3.up, transform);
        moveDown = new MoveCommand(Vector3.down, transform);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            moveLeft.Execute();

        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            moveRight.Execute();
            history.Push(moveRight);
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            moveUp.Execute();
            history.Push(moveUp);
           
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            moveDown.Execute();
            history.Push(moveDown);
            
        }

        if (Input.GetKeyDown(KeyCode.Backspace))
        {
            history.Pop().Undo();
            
        }
    }
    
}
