﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCommand :  Icommands
{
    private Vector3 direction;
    public Transform transform;

    public MoveCommand(Vector3 direction, Transform transform)
    {
        this.direction = direction;
        this.transform = transform;
    }
    public void Execute()
    {
        transform.position += direction;
    }

    public void Undo()
    {
        transform.position -= direction;
    }

    public void Redo()
    {
        transform.position += direction;
    }
}
